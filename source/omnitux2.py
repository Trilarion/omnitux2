# Omnitux 2 - educational activities based upon multimedia elements
# Copyright (C) 2014 Trilarion
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

'''
    Start with working directory equals base directory of project.
'''

import json
import shutil
import os
import sys

from PySide import QtCore, QtGui, QtSvg

import aux

class Options():
    def __init__(self, file=None):
        self.options = {}
        # already read if necessary
        if file is not None:
            self.load(file)

    def load(self, file):
        with open(file, 'r') as f:
            self.options = json.load(f)

    def save(self, file):
        with open(file, 'w') as f:
            json.dump(self.options, f, indent=2, separators=(',', ': '))

    def get(self, key):
        return self.options[key]

    def set(self, key, value):
        self.options[key] = value

class ButtonSvg(QtSvg.QGraphicsSvgItem):
    def __init__(self, fileName, mousePressedCallback=None):
        QtSvg.QGraphicsSvgItem.__init__(self, fileName)
        self.mousePressedCallback = mousePressedCallback
        self.setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))

    def enterEvent(self, event):
        pass

    def leaveEvent(self, event):
        pass

    def mousePressEvent(self, event):
        if self.mousePressedCallback is not None:
            self.mousePressedCallback()

if __name__ == '__main__':

    # determine home dir
    folder = 'Omnitux2'
    if os.name == 'posix':
        # Linux / Unix
        home_dir = os.path.join(os.getenv('HOME'), folder)
    elif (os.name == 'nt') and (os.getenv('USERPROFILE') is not None):
        # MS Windows
        home_dir = os.path.join(os.getenv('USERPROFILE'), folder)
    else:
        home_dir = '..'

    # if not exists, create home dir
    if not os.path.isdir(home_dir):
        os.mkdir(home_dir)

    # redirect output to log files (will be overwritten each time)
    log_file = os.path.join(home_dir, 'omnitux2.log')
    # sys.stdout = codecs.open(log_file, encoding='utf-8', mode='w')
    err_file = os.path.join(home_dir, 'omnitux2.error.log')
    # sys.stderr = codecs.open(err_file, encoding='utf-8', mode='w')
    aux.info('Omnitux 2 up and running..')

    # search for existing options file
    options_file = os.path.join(home_dir, 'omnitux2.options')
    if not os.path.exists(options_file):
        default_options_file = aux.resource('options.default.json')
        shutil.copyfile(default_options_file, options_file)
    options = Options(options_file)
    aux.info('Version {}'.format(options.get('general.version')))

    # create GUI application and run
    app = QtGui.QApplication(sys.argv)

    # set default font
    id = QtGui.QFontDatabase.addApplicationFont(aux.resource('resources', 'fonts', 'FreeSans.ttf'))
    if id == -1:
        pass  # TODO error (non fatal), could not load font
    fontFamilies = QtGui.QFontDatabase.applicationFontFamilies(id)
    app.setFont(QtGui.QFont(fontFamilies[0]))

    # create main window
    main_window = QtGui.QGraphicsView()
    main_window.showFullScreen()
    # size = main_window.childrenRect()
    main_window.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
    main_window.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
    size = main_window.size()
    main_window.setAlignment(QtCore.Qt.AlignCenter)

    # load splash image
    splash_image = QtGui.QPixmap(aux.resource('Omnitux2_logo.png'))

    # show splash screen
    splash_screen = QtGui.QSplashScreen(main_window, splash_image)
    splash_screen.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
    splash_screen.show()

    e = QtGui.QGraphicsDropShadowEffect()
    e.setBlurRadius(10)
    splash_screen.setGraphicsEffect(e)

    # add opacity effect
    effect = QtGui.QGraphicsOpacityEffect()
    effect.setOpacity(1)
    splash_screen.setGraphicsEffect(effect)

    # create animation
    animation = QtCore.QPropertyAnimation(effect, 'opacity')
    animation.setDuration(1000)  # 2 seconds duration
    animation.setStartValue(1)
    animation.setEndValue(0)
    animation.finished.connect(splash_screen.close)  # need to properly close the splash scren

    # start in 3 seconds
    QtCore.QTimer.singleShot(2000, animation, QtCore.SLOT('start()'))

    # set scene
    scene = QtGui.QGraphicsScene()
    background_image = QtGui.QPixmap(aux.resource('resources', 'backgrounds', 'GPN-2001-000009_modified.jpg'))
    # item = scene.addPixmap(background_image.scaled(size))
    item = scene.addPixmap(background_image)

    print(scene.itemsBoundingRect())

    main_window.setScene(scene)

    rcontent = main_window.contentsRect()
    main_window.setSceneRect(0, 0, rcontent.width(), rcontent.height())
    main_window.setContentsMargins(QtCore.QMargins())
    main_window.setViewportMargins(QtCore.QMargins())
    # main_window.setStyleSheet("border: 0px")
    main_window.setStyleSheet("border-width: 0px; border-style: solid")
    main_window.setBackgroundBrush(QtGui.QBrush(QtGui.QColor(0, 0, 0)))

    button = ButtonSvg(aux.resource('dummy_button.svg'), main_window.close)
    button.setPos(1000, 200)
    scene.addItem(button)
    print(button.renderer().defaultSize())
    print(button.renderer().viewBoxF())
    print(button.pos())
    print(button.boundingRect())
    print(button.scenePos())
    print(button.sceneBoundingRect())

    sys.exit(app.exec_())