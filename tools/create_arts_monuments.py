# Omnitux 2 - educational activities based upon multimedia elements
# Copyright (C) 2014 Trilarion
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import json
import os
from aux import lang

def create_file_path(folder):
    return os.path.join('..', 'data', 'games', 'arts', 'monuments', folder, 'game.info')

def italian():
    d = {}
    d['title'] = lang('it', 'Monumenti italiani')
    d['thumbnail'] = 'Pisa_Duomo_2.jpg'

    k = []

    i = {}
    i['names'] = lang('it', 'Assisi (PG) Umbria - San Pietro')
    i['image'] = 'FacadeS.Pietro_Assisi.jpg'
    k.append(i)

    i = {}
    i['names'] = lang('it', 'Firenze Toscana - Uffizi e Palazzo Vecchio')
    i['image'] = 'FirenzeIMG0281_bordercropped.jpg'
    k.append(i)

    d['items'] = k

    # save
    file = open(create_file_path('italian'), 'w')
    json.dump(d, file, indent=2, separators=(',', ': '))


if __name__ == "__main__":
    italian()