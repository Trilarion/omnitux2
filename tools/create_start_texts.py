# Omnitux 2 - educational activities based upon multimedia elements
# Copyright (C) 2014 Trilarion
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import os
import aux as t

def create_file_path(folder):
    return os.path.join('..', 'data', 'games', folder, 'category.info')


def mouse_manipulation():
    d = {}
    d['title'] = t.lang('de', 'Maus Manipulation', 'en', 'Mouse manipulation', 'es',
                      'Manipulación del ratón', 'fr', 'Manipulation de la souris',
                      'it', 'Utilizzo del Mouse')

    # save
    t.write(create_file_path('mouse_manipulation'), d)

def learning():
    d = {}
    d['title'] = t.lang('de', 'Lernen', 'en', 'Learning')

    # save
    t.write(create_file_path('learning'), d)

def associate():
    d = {}
    d['title'] = t.lang('de', 'Assoziationen', 'en', 'Associations')

    # save
    t.write(create_file_path('associate'), d)

def memory():
    d = {}
    d['title'] = t.lang('de', 'Memory', 'en', 'Memory cards')

    # save
    t.write(create_file_path('memory'), d)

def puzzle():
    d = {}
    d['title'] = t.lang('de', 'Puzzles', 'en', 'Jigsaw puzzles')

    # save
    t.write(create_file_path('puzzle'), d)

def differences():
    d = {}
    d['title'] = t.lang('de', 'Unterschiede', 'en', 'Differences')

    # save
    t.write(create_file_path('differences'), d)

def numbers():
    d = {}
    d['title'] = t.lang('de', 'Zählen', 'en', 'Numbers')

    # save
    t.write(create_file_path('numbers'), d)

def writing():
    d = {}
    d['title'] = t.lang('de', 'Schreiben', 'en', 'Writing')

    # save
    t.write(create_file_path('writing'), d)

def arts():
    d = {}
    d['title'] = t.lang('de', 'Künste', 'en', 'Arts')

    # save
    t.write(create_file_path('arts'), d)

def geography():
    d = {}
    d['title'] = t.lang('de', 'Geographie', 'en', 'Geography')

    # save
    t.write(create_file_path('geography'), d)

if __name__ == "__main__":
    mouse_manipulation()
    learning()
    associate()
    memory()
    puzzle()
    differences()
    numbers()
    writing()
    arts()
    geography()