# -*- coding: utf-8 -*-

# Omnitux - educational activities based upon multimedia elements
# Copyright (C) 2010 Olav_2 (olav.olav@yahoo.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import os, sys, tarfile, shutil, stat

sys.path.append("../../bin")		
import common, constants, datetime


par_maintainer_email = "Olav_2 <olav_2@users.sourceforge.net>"
par_homepage = "http://omnitux.sourceforge.net/"
par_description = "educational activities based on multimedia elements.\n" +\
" .\n"+\
" Types of activities:\n"+\
" .\n"+\
"  * associations,\n"+\
"  * items to place on a map or a schema,\n"+\
"  * counting activities,\n"+\
"  * puzzles,\n"+\
"  * card faces to remember,\n"+\
"  * find differences between two pictures,\n"+\
"  * ...\n"+\
" .\n"+\
" Activities are available in English, French, German, Polish, Portuguese, Spanish and Italian.\n"

par_summary = "educational activities based on multimedia elements"


# recursive file copy
def copy_files(light_mode, srcpath, destpath) :

	entries = os.listdir(srcpath)

	for entry in entries :

		entry_with_path = os.path.join(srcpath, entry)

		if (os.path.isdir(entry_with_path)) :
			if (entry not in ("tools", "website", ".svn")) :
				# we exclude tools, website and .svn directories
				if (cmp(entry[0], ".") != 0) :
					# we exclude any Linux hidden directory

					os.mkdir(destpath+"/"+entry)

					copy_files(light_mode, entry_with_path, destpath+"/"+entry+"/")

		elif (os.path.isfile(entry_with_path)) :
			if (cmp(entry[0], ".") != 0) :
				# we exclude hidden files

				# by default, we won't add the file
				copy_file = False

				(entry_nolast_ext, last_ext) = os.path.splitext(entry)
				(entry_noexts, ext_inside) = os.path.splitext(entry_nolast_ext)
	
				if (cmp(ext_inside, ".light") == 0) :
					if (light_mode == True) :
						# a light file is being added only in light mode
						copy_file = True

				else :
					# this is not a 'light' file
					if (light_mode == False) :
						copy_file = True
					else :
						# in case light mode is 'on'
						# we have to make sure this file does not have a light file
						if (os.path.isfile(common.light_name(os.path.join(srcpath, entry))) == False) :
							copy_file = True



				if (copy_file == True) :
					# print "Copying file : " + entry_with_path

					#print entry_with_path
					#print destpath+"/"+entry
	
					shutil.copy(entry_with_path, destpath+"/"+entry)


def move_and_copy(light_mode, path) :
	
	os.mkdir(path+"usr")
	os.mkdir(path+"usr/share")

	if (light_mode == True) :
		share_omnitux = path+"usr/share/omnitux-light/"
	else :
		share_omnitux = path+"usr/share/omnitux/"

	os.mkdir(share_omnitux)

	os.rename(path+"data", share_omnitux+"data")
	os.rename(path+"bin", share_omnitux+"bin")
	os.rename(path+"log", share_omnitux+"log")

	os.rename(path+"LICENSE.txt", share_omnitux+"LICENSE.txt")	
	os.rename(path+"README.txt", share_omnitux+"README.txt")	
	os.rename(path+"omnitux_setup.sh", share_omnitux+"omnitux_setup.sh")

	if (light_mode == True) :
		os.rename(path+"omnitux-light.sh", share_omnitux+"omnitux-light.sh")	
	else :
		os.rename(path+"omnitux.sh", share_omnitux+"omnitux.sh")


	# create .desktop file

	os.mkdir(path+"/usr/share/applications")
	
	if (light_mode == True) :
		desktop_filename = path+"/usr/share/applications/omnitux-light.desktop"
	else :
		desktop_filename = path+"/usr/share/applications/omnitux.desktop"

	desktop = open(desktop_filename, "w")

	desktop.write("[Desktop Entry]\n")
	desktop.write("Encoding=UTF-8\n")

	if (light_mode == True) :
		desktop.write("Path=/usr/share/omnitux-light\n")
		desktop.write("Exec=/usr/bin/omnitux-light\n")
	else :
		desktop.write("Path=/usr/share/omnitux\n")
		desktop.write("Exec=/usr/bin/omnitux\n")	

	desktop.write("Type=Application\n")
	desktop.write("Categories=Game;Education;\n")

	if (light_mode == True) :
		desktop.write("Name=Omnitux-light\n")
		desktop.write("GenericName=Omnitux-light\n")
	else :
		desktop.write("Name=Omnitux\n")
		desktop.write("GenericName=Omnitux\n")


	desktop.write("Comment=An educational game based on multimedia elements.\n")
	desktop.write("Comment[fr]=Un jeu ludo-éducatif basé sur des éléments multimédias.\n")
	desktop.write("Terminal=false\n")

	if (light_mode == True) :
		desktop.write("Icon=/usr/share/omnitux-light/data/default/icons/Omnitux_logo.light.svg\n")
	else :
		desktop.write("Icon=/usr/share/omnitux/data/default/icons/Omnitux_logo.svg\n")

	desktop.close()

	# create startup shell for /usr/bin

	os.mkdir(path+"/usr/bin")

	if (light_mode == True) :
		shell_filename = path+"/usr/bin/omnitux-light"
	else :
		shell_filename = path+"/usr/bin/omnitux"

	shell = open(shell_filename, "w")

	shell.write("#!/bin/sh\n")
	shell.write("\n")

	if (light_mode == True) :
		shell.write("cd /usr/share/omnitux-light/bin\n")
	else :
		shell.write("cd /usr/share/omnitux/bin\n")

	shell.write("\n")

	shell.write("python menu.py $*\n")

	shell.close()

	os.chmod(shell_filename, stat.S_IRUSR | stat.S_IXUSR | stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH | stat.S_IXOTH)
	

def control_install(light_mode, workdir, distribution) :
	# TODO put a real calculation of estimated sizes
	complete_size = 165 * 1024
	light_size = 95 * 1024


	os.mkdir(workdir+"DEBIAN")

	if (light_mode == True) :
		dirname = "omnitux-light"
	else :
		dirname = "omnitux"

	shutil.copy(workdir+"usr/share/"+dirname+"/LICENSE.txt", workdir+"DEBIAN/copyright")	
	
	control_install = open(workdir+"DEBIAN/control", "w")

	if (light_mode == True) :
		control_install.write("Package: omnitux-light\n")
	else :
		control_install.write("Package: omnitux\n")

	control_install.write("Version: "+constants.version+"\n")

	control_install.write("Section: games\n")
	control_install.write("Priority: optional\n")
	control_install.write("Architecture: all\n")

	if (light_mode == True) :
		est_size = light_size
	else :
		est_size = complete_size

	control_install.write("Installed-Size: "+str(est_size)+"\n")

	control_install.write("Depends: python-pygame (>=1.7), python-gtk2 (>=2.8)\n")
	control_install.write("Maintainer: "+par_maintainer_email+"\n")
	control_install.write("Homepage: "+par_homepage+"\n")
	control_install.write("Description: " + par_description+"\n")
	control_install.close()

def specfile(light_mode, workdir, distribution) :
	
	if (cmp(distribution, "openSUSE") == 0) :
		spec = open(workdir+"openSUSE.spec", "w")
	elif (cmp(distribution, "Fedora") == 0) :
		spec = open(workdir+"Fedora.spec", "w")

	if (light_mode == True) :
		spec.write("Name: omnitux-light\n")
	else :
		spec.write("Name: omnitux\n")

	spec.write("Version: "+constants.version+"\n")
	spec.write("Release: 1\n")
	spec.write("Summary: "+par_summary+"\n")
	spec.write("URL: "+par_homepage+"\n")
	spec.write("License: GPL and other free licenses\n")
	spec.write("Group: Amusements/Teaching/Other\n")
	spec.write("Packager: "+par_maintainer_email+"\n")

	if (cmp(distribution, "openSUSE") == 0) :
		spec.write("Requires: python-pygame >= 1.7, python-gtk >= 2.8\n")	
	elif (cmp(distribution, "Fedora") == 0) :
		spec.write("Requires: pygame >= 1.7, pygtk2 >= 2.8")

#	spec.write("BuildArchitectures: noarch\n")

#	if (light_mode == True) :
#		spec.write("BuildRoot: "+workdir+"omnitux-light/")
#	else :
#		spec.write("BuildRoot: "+workdir+"omnitux/")

	spec.write("\n")
	spec.write("\n")

	spec.write("%description\n")
	spec.write("\n")

	spec.write(par_description+"\n")

	spec.write("%files\n")
	spec.write("%defattr(-,root,root)\n")
	spec.write("/usr\n")

	spec.close()

#
# Main
#

# creating buildsrc_dir+"omnitux" folder
version = constants.version

startpath = os.getcwdu()

build_dir = startpath + "/../../../build/"+version+"-"+datetime.datetime.now().isoformat()+"/"
src_dir = build_dir+"/src/"

os.mkdir(build_dir)
build_light_dir = build_dir + "light/"
build_complete_dir = build_dir + "complete/"
os.mkdir(build_light_dir)
os.mkdir(build_complete_dir)


os.mkdir(src_dir)
src_light_dir = src_dir+"light/"
src_complete_dir = src_dir+"complete/"
os.mkdir(src_light_dir)
os.mkdir(src_complete_dir)


src_light_tar_dir = src_light_dir+"tar/"
os.mkdir(src_light_dir+"deb")
src_light_deb_ubuntu_dir = src_light_dir+"deb/Ubuntu/"

os.mkdir(src_light_dir+"rpm")
src_light_rpm_suse_dir = src_light_dir + "rpm/openSUSE/"
src_light_rpm_fedora_dir = src_light_dir + "rpm/Fedora/"

os.mkdir(src_light_tar_dir)
os.mkdir(src_light_deb_ubuntu_dir)
os.mkdir(src_light_rpm_suse_dir)
os.mkdir(src_light_rpm_fedora_dir)


src_complete_tar_dir = src_complete_dir+"tar/"
os.mkdir(src_complete_dir+"deb")
src_complete_deb_ubuntu_dir = src_complete_dir+"deb/Ubuntu/"

os.mkdir(src_complete_dir+"rpm")
src_complete_rpm_suse_dir = src_complete_dir + "rpm/openSUSE/"
src_complete_rpm_fedora_dir = src_complete_dir + "rpm/Fedora/"

os.mkdir(src_complete_tar_dir)
os.mkdir(src_complete_deb_ubuntu_dir)
os.mkdir(src_complete_rpm_suse_dir)
os.mkdir(src_complete_rpm_fedora_dir)


print

print "############################"
print "## BUILDING OMNITUX " + version + " ##"
print "############################"

print

print "[1] Prepare contents"

print "[1-A] for tar.bz2 light"
os.mkdir(src_light_tar_dir+"omnitux-light")
copy_files(True, "../../../omnitux", src_light_tar_dir+"/omnitux-light")

os.rename(src_light_tar_dir+"/omnitux-light/omnitux.sh", src_light_tar_dir+"/omnitux-light/omnitux-light.sh")

release_light = open(src_light_tar_dir+"/omnitux-light/bin/"+constants.release_file, "w")
release_light.write("light\n");
release_light.close()

print "[1-B] for tar.bz2 complete"
os.mkdir(src_complete_tar_dir+"omnitux")
copy_files(False, "../../../omnitux", src_complete_tar_dir+"/omnitux")

release_complete = open(src_complete_tar_dir+"/omnitux/bin/"+constants.release_file, "w")
release_complete.write("complete\n");
release_complete.close()

print "[1-C] for deb light (Ubuntu)"
shutil.copytree(src_light_tar_dir+"/omnitux-light", src_light_deb_ubuntu_dir+"/omnitux-light")

move_and_copy(True, src_light_deb_ubuntu_dir+"/omnitux-light/")
control_install(True, src_light_deb_ubuntu_dir+"/omnitux-light/", "Ubuntu")


print "[1-D] for deb complete (Ubuntu)"
shutil.copytree(src_complete_tar_dir+"/omnitux", src_complete_deb_ubuntu_dir+"/omnitux")

move_and_copy(False, src_complete_deb_ubuntu_dir+"/omnitux/")
control_install(False, src_complete_deb_ubuntu_dir+"/omnitux/", "Ubuntu")

print "[1-E] for rpm light (openSUSE)"
shutil.copytree(src_light_tar_dir+"/omnitux-light", src_light_rpm_suse_dir+"/omnitux-light")

move_and_copy(True, src_light_rpm_suse_dir+"/omnitux-light/")
specfile(True, src_light_rpm_suse_dir, "openSUSE")

print "[1-F] for rpm complete (openSUSE)"
shutil.copytree(src_complete_tar_dir+"/omnitux", src_complete_rpm_suse_dir+"/omnitux")

move_and_copy(False, src_complete_rpm_suse_dir+"/omnitux/")
specfile(False, src_complete_rpm_suse_dir, "openSUSE")


print "[1-G] for rpm light (Fedora)"
shutil.copytree(src_light_tar_dir+"/omnitux-light", src_light_rpm_fedora_dir+"/omnitux-light")

move_and_copy(True, src_light_rpm_fedora_dir+"/omnitux-light/")
specfile(True, src_light_rpm_fedora_dir, "Fedora")

print "[1-H] for rpm complete (Fedora)"
shutil.copytree(src_complete_tar_dir+"/omnitux", src_complete_rpm_fedora_dir+"/omnitux")

move_and_copy(False, src_complete_rpm_fedora_dir+"/omnitux/")
specfile(False, src_complete_rpm_fedora_dir, "Fedora")

#
# TAR BZ.2
#

print
print "[2] Generation !"

print "[2-A] Generating tar.bz2 light package"


tarfile_handle = tarfile.open(build_light_dir + "omnitux-light-"+version+".tar.bz2", "w:bz2", None , tarfile.PAX_FORMAT)

os.chdir(src_light_tar_dir)

tarfile_handle.add(".")

tarfile_handle.close()


print "[2-B] Generating tar.bz2 complete package"


tarfile_handle = tarfile.open(build_complete_dir + "omnitux-"+version+".tar.bz2", "w:bz2", None , tarfile.PAX_FORMAT)

os.chdir(src_complete_tar_dir)

tarfile_handle.add(".")

tarfile_handle.close()

#
# DEB for Ubuntu
#


print "[2-C] Generating light DEB package for Ubuntu"

os.system("dpkg-deb --build "+src_light_deb_ubuntu_dir+"/omnitux-light "+build_light_dir+"omnitux-light_"+version+"_all.deb")


print "[2-D] Generating complete DEB package for Ubuntu"

os.system("dpkg-deb --build "+src_complete_deb_ubuntu_dir+"/omnitux "+build_complete_dir+"omnitux_"+version+"_all.deb")


#
# RPM for openSUSE
#

print "[2-E] Generating light RPM package for openSUSE"

os.system("rpmbuild -bb --target noarch --buildroot "+src_light_rpm_suse_dir+"omnitux-light/  "+src_light_rpm_suse_dir+"openSUSE.spec")

shutil.copy(os.environ['HOME']+"/rpmbuild/RPMS/noarch/omnitux-light-"+version+"-1.noarch.rpm", build_light_dir+"omnitux-light-"+version+"-1.noarch-openSUSE.rpm")


print "[2-F] Generating complete RPM package for openSUSE"

os.system("rpmbuild -bb --target noarch --buildroot "+src_complete_rpm_suse_dir+"omnitux/  "+src_complete_rpm_suse_dir+"openSUSE.spec")

shutil.copy(os.environ['HOME']+"/rpmbuild/RPMS/noarch/omnitux-"+version+"-1.noarch.rpm", build_complete_dir+"omnitux-"+version+"-1.noarch-openSUSE.rpm")


#
# RPM for Fedora
#

print "[2-G] Generating light RPM package for Fedora"

os.system("rpmbuild -bb --target noarch --buildroot "+src_light_rpm_fedora_dir+"omnitux-light/  "+src_light_rpm_fedora_dir+"Fedora.spec")

shutil.copy(os.environ['HOME']+"/rpmbuild/RPMS/noarch/omnitux-light-"+version+"-1.noarch.rpm", build_light_dir+"omnitux-light-"+version+"-1.noarch-Fedora.rpm")


print "[2-H] Generating complete RPM package for Fedora"

os.system("rpmbuild -bb --target noarch --buildroot "+src_complete_rpm_fedora_dir+"omnitux/  "+src_complete_rpm_fedora_dir+"Fedora.spec")

shutil.copy(os.environ['HOME']+"/rpmbuild/RPMS/noarch/omnitux-"+version+"-1.noarch.rpm", build_complete_dir+"omnitux-"+version+"-1.noarch-Fedora.rpm")


# export DEBFULLNAME="Olav_2"

# dh_make -c gpl2 -s -p omnitux-light_1.2.1 -e "olav_2 at users.sourceforge.net" -i

# dh_make -c gpl2 -s -p omnitux-light_1.2.1 -e "olav_2 at users.sourceforge.net" -i  -f ../../../../light/omnitux-light-1.2.1.tar 

# dh_make -c gpl2 -s -p omnitux-light_1.2.1 -e "olav_2@users.sourceforge.net" -i  

