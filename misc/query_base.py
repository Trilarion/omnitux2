# -*- coding: utf-8 -*-

# Omnitux - educational activities based upon multimedia elements
# Copyright (C) 2009 Olav_2 (olav.olav@yahoo.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import pygame, sys, os, random, copy, re, datetime, xml.dom, xml.dom.minidom, codecs, traceback, codecs
from pygame.locals import *

sys.path.append("../bin")		
import common, constants, xml_funcs, i18n

conn = None

print
print "=== Queries in omnitux database ==="
print

try :
	mysql = True
	import MySQLdb

	conn = MySQLdb.connect (host = "localhost",
                           user = "%",
                           passwd = "",
                           db = "o242263_omnitux")


	cursor = conn.cursor ()


	cursor.execute("SET NAMES \'utf8\'")


except Exception, e :
	mysql = False

	print e

sys.stdout.write("Availability of mysql connexion : ")
print mysql


if (mysql == True) :
	cursor.execute( "select * from authors " + \
			"where authors.author_name != \"undefined\" and authors.author_name != \"?\" " + \
			"order by authors.author_name")

	rows = cursor.fetchall ()
	for row in rows:

		print row[0].capitalize()


	cursor.close()
	
