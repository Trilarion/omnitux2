# -*- coding: utf-8 -*-

# Omnitux - educational activities based upon multimedia elements
# Copyright (C) 2009 Olav_2 (olav.olav@yahoo.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import pygame, sys, os, random, copy, re, datetime, xml.dom, xml.dom.minidom, codecs, traceback, codecs
from pygame.locals import *

sys.path.append("../bin")		
import common, constants, xml_funcs, i18n, ui


csvsep = ";"



class MediaFile() :

	def __init__(self, path, filename, mediatype) :

		self.path = path

		self.filename = filename

		self.key = os.path.join(path, filename)

		self.mediatype = mediatype

		if (cmp(self.mediatype, "image") == 0) :
			(self.imgtype, self.width, self.height) = ui.get_image_info(self.key)

			self.imgtype = self.imgtype["name"]

		self.size = os.path.getsize(self.key)

		self.size_light = ""
		self.width_light = ""
		self.height_light = ""

		filename_light = common.light_name(self.key)

		if (os.path.isfile(filename_light)) :
			self.size_light = os.path.getsize(filename_light)

			if (cmp(self.mediatype, "image") == 0) :
				(foo, self.width_light, self.height_light) = ui.get_image_info(filename_light)



	def print_mediafile(self) :

		string = self.path + csvsep + self.filename + csvsep + self.mediatype  + csvsep + str(self.size) + csvsep + str(self.size_light)

		if (cmp(self.mediatype, "image") == 0) :

			string = string + csvsep + self.imgtype + csvsep + str(self.width) + csvsep + str(self.height) \
				+ csvsep + str(self.width_light) + csvsep + str(self.height_light)

		return string

class MediaFiles() :

	def __init__(self) :
		
		self.mediafiles = []

	def add(self, path, filename, mediatype) :

		(entry_nolast_ext, last_ext) = os.path.splitext(filename)
		(entry_noexts, ext_inside) = os.path.splitext(entry_nolast_ext)

		if (cmp(ext_inside, ".light") == 0) :
			filename = entry_noexts + last_ext
		
		key = os.path.join(path, filename)

		for mediafile in self.mediafiles :

			if (cmp(mediafile.key, key) == 0) :
				return mediafile

		# if we get here, this means that's the first time we get this mediafile
		mediafile = MediaFile(path, filename, mediatype)

		self.mediafiles.append(mediafile)

		return mediafile


	def print_mediafiles(self) :

		print
		print "Saving media list in file " + os.path.join(constants.home_dir, "media_files.csv")
		print

		f = open(os.path.join(constants.home_dir, "media_files.csv"), "w")

		string =  "Path"+csvsep+"Media file"+csvsep+"Type"+csvsep+"Size"+csvsep+"Size light" + csvsep+ "Width"+csvsep+"Height"+ csvsep+ "Width light"+csvsep+"Height light"  
		
		f.write(string+"\n")

		for mediafile in self.mediafiles :

			string = mediafile.print_mediafile()

			f.write(string+"\n")
	
		f.close()


class Activity() :

	def __init__(self, path, activity_name) :

		# TODO : add image counting by language

		self.path = path 
		self.activity_name = activity_name

		self.key = path+"/"+activity_name

		# a language is "activated" when there is a <title>
		# with the lang attribute is found
		self.active_langs = set()

		self.texts = {}
		self.titles = {}
		self.i18n_dict = None
		self.sounds = {}

		for lang in constants.supported_languages :
			self.texts[lang] = []
			self.sounds[lang] = []
			self.titles[lang] = []

		self.texts["?"] = []
		self.sounds["?"] = []
		self.titles["?"] = []

	def add_title(self, lang, title) :
		if (cmp(lang, "") == 0) :
			lang = "?"

		self.active_langs.add(lang)
		self.titles[lang].append(title)

	def add_text(self, lang, text) :
		if (cmp(lang, "") == 0) :
			lang = "?"

		self.texts[lang].append(text)

	def add_sound(self, lang, sound) :
		if (cmp(lang, "") == 0) :
			lang = "?"

		self.sounds[lang].append(sound)


class Activities(): 
	def __init__(self) :
		self.activities = []

	def add(self, path, activity_name) :
		
		key = path+"/"+activity_name

		for activity in self.activities :

			if (cmp(activity.key, key) == 0) :
				return activity

		# if we get here, this means that's the first time we get this activity
		activity = Activity(path, activity_name)

		self.activities.append(activity)

		return activity

	def print_active_langs(self) :

		print
		print "Saving active languages in file " + os.path.join(constants.home_dir, "active_languages.csv")
		print

		f = open(os.path.join(constants.home_dir, "active_languages.csv"), "w")

		string =  "Path;Activity name"
		for lang in constants.supported_languages :
			string = string + ";"+lang
		
		f.write(string+"\n")


		for activity in self.activities :
			string = activity.path + ";" + \
			activity.activity_name
			
			for lang in constants.supported_languages :
				string = string + ";"
				if (lang in activity.active_langs) :
					string =  string + "Y"
				else :
					string = string + "N"

			f.write(string+"\n")

		f.close()


	def print_lang_stats(self) :

		print
		print "Saving language stats in file " + os.path.join(constants.home_dir, "lang_stats.csv")
		print

		f = open(os.path.join(constants.home_dir, "lang_stats.csv"), "w")


		string =  "Path;Activity name"
		for lang in constants.supported_languages :
			string = string + ";"+lang+"-titles"
		for lang in constants.supported_languages :
			string = string + ";"+lang+"-dict"
		for lang in constants.supported_languages :
			string = string + ";"+lang+"-texts"
		for lang in constants.supported_languages :
			string = string + ";"+lang+"-sounds"


		f.write(string+"\n")


		for activity in self.activities :
			string = activity.path + ";" + \
			activity.activity_name
			
			for lang in constants.supported_languages :
				string = string + ";"
				if (lang in activity.active_langs) :
					string =  string + str(len(activity.titles[lang])) 
				else :
					string = string + "0"

			for lang in constants.supported_languages :
				string = string + ";"
				if (lang in activity.active_langs) :
					string =  string + str(len(activity.i18n_dict.text_dict[lang])) 
				else :
					string = string + "0"

			for lang in constants.supported_languages :
				string = string + ";"
				if (lang in activity.active_langs) :
					string =  string + str(len(activity.texts[lang])) 
				else :
					string = string + "0"

			for lang in constants.supported_languages :
				string = string + ";"
				if (lang in activity.active_langs) :
					string =  string + str(len(activity.sounds[lang])) 
				else :
					string = string + "0"


			f.write(string+"\n")

		f.close()


conn = None

print
print "=== Analysis of omnitux data files ==="
print

try :
	mysql = True
	import MySQLdb

	conn = MySQLdb.connect (host = "localhost",
                           user = "%",
                           passwd = "",
                           db = "o242263_omnitux")


	cursor = conn.cursor ()


	cursor.execute("SET NAMES \'utf8\'")


except Exception, e :
	mysql = False

	print e

sys.stdout.write("Availability of mysql connexion : ")
print mysql


if (mysql == True) :
	print "Cleaning the database"
	cursor.callproc("Reset_base")



media_types = ["sound", "image", "xml"]

media_tags = ["image", "sound", "thumbnail", "background", "front", "back"]

constant_undefined = "undefined"


extensions = {}

extensions["xml"] = ["xml", "XML"]

extensions["sound"] = ["ogg"]

extensions["image"] = ["jpg", "gif", "svg", "png", "JPG", "GIF", "SVG", "PNG"]


original_path = os.getcwd()

total_multimedia_files = 0.0

total_multimedia_files_with_lic_info = 0.0


mediafiles = MediaFiles()
activities = Activities()


for data_folder in constants.data_folders :

	os.chdir(os.path.join(original_path, data_folder))

	for media_type in media_types :

		data_files = common.get_files(".", extensions[media_type])

		for data_file in data_files :

			(path, filename) = os.path.split(data_file)

			(data_file_with_no_extension, extension) = os.path.splitext(data_file)


			par_file_original_URL = constant_undefined
			par_file_license_URL = constant_undefined
			par_author_name = constant_undefined
			par_license_name = constant_undefined

			file_languages = []

			if (media_type != "xml") :

				(entry_nolast_ext, last_ext) = os.path.splitext(data_file)
				(entry_noexts, ext_inside) = os.path.splitext(entry_nolast_ext)

				if (cmp(ext_inside, ".light") != 0) :

					# Try to get the license, author, etc of the media file
					license_file = data_file + ".txt"

					if (os.path.isfile(license_file) == False) :
						license_file = os.path.join(path, "default.txt")


					if (os.path.isfile(license_file) == True) :

						#####sys.stdout.write(".")

						# License file formats (xxx.ext.txt or default.txt) :

						# 4 lines :
						# url_file, url_license, author, license

						# 3 lines :
						# url_license, author, license

						# 2 lines :
						# author, license


						lines = []

						FILE = open(license_file,"r")

						for line in FILE.readlines() :
							
							line = line.rstrip(' \r\n')

							if len(line) > 0 :
								lines.append(line)


						FILE.close()


						# if (len(lines) == 1) :

							# print "Fixing txt for " + data_file
		
							# (name, foo, foo) = filename.partition("-")

							# lines.append(name)

							# lines.append("Creative Commons BY-NC-SA")
							
							# FILE = open(license_file,"w")

							#FILE.write(lines[0]+"\n")
							#FILE.write(lines[1]+"\n")
							#FILE.write(lines[2]+"\n")

							#FILE.close()


						if (len(lines) == 4) :
							par_file_original_URL = lines[0]
							par_file_license_URL = lines[1]
							par_author_name = lines[2]
							par_license_name = lines[3]

						elif (len(lines) == 3) :
							par_file_original_URL = constant_undefined
							par_file_license_URL = lines[0]
							par_author_name = lines[1]
							par_license_name = lines[2]

						elif (len(lines) == 2) :
							par_file_original_URL = constant_undefined
							par_file_license_URL = constant_undefined
							par_author_name = lines[0]
							par_license_name = lines[1]						

						else :
							print data_file + " has incorrect license information"



						total_multimedia_files_with_lic_info = total_multimedia_files_with_lic_info + 1

					else :
						print data_file + " has no license information"


				# now trying to guess the language of the media file



				if (len(filename) > 3) :
					third_char = filename[2]
	
					if ((third_char == "_") or (third_char == "-")) :

						potential_language = filename[0:2]
						
						potential_language = potential_language.lower()

						if (potential_language in constants.supported_languages) :
							# this should be a good guess !

							file_languages.append(potential_language)


				# adding the data file

				mediafiles.add(path, filename, media_type) 

			else :
				# in case we have an XML activity file
				try :

					if ((cmp(filename[2], '-') == 0) or (cmp(filename[2], '_') == 0)) :
						activity_name = filename[3:len(filename)-4]
					else :
						activity_name = filename[0:len(filename)-4]


					activity = activities.add(path, activity_name)

					game_set = common.Game_set(data_file)

					activity.i18n_dict = game_set.i18n_dict

					# try to read the author of the XML activity
					par_author_name = game_set.get_author()

					if (par_author_name == "") :
						par_author_name = constant_undefined
						print data_file + " has no known author"
	
					else :
						# TODO : put a tag in the XML file and read it
						par_license_name = "CC-BY-SA"

						total_multimedia_files_with_lic_info = total_multimedia_files_with_lic_info + 1


					# listing the media files used by this file

					for media_tag in media_tags :

						nodes = game_set.xml_data.getElementsByTagName(media_tag)

						for node in nodes :

							mediafile = os.path.join(".", xml_funcs.getText(node))

							if (mysql == True) :
								cursor.callproc("xmlfile_uses_mediafile", (data_file, mediafile, media_tag))

								##### sys.stdout.write("=")

								sys.stdout.flush()



					# reading the title(s)
					title_nodes = game_set.xml_data.getElementsByTagName("title")

					titles = {}

					langs = set()

					for title_node in title_nodes :
						title = xml_funcs.getText(title_node)
						lang = title_node.getAttribute("lang")

						titles[lang] = title

						langs.add(lang)

						activity.add_title(lang, title)

					default_lang = ""

					if (len(langs) == 1) :
						# we're inside a single language file
						# so we assume the language of the text is the same as the title
						for lang in langs :
							default_lang = lang
					


					# reading the text elements

					text_nodes = game_set.xml_data.getElementsByTagName("text")

					for text_node in text_nodes :
						text = xml_funcs.getText(text_node, game_set.i18n_dict)
						lang = text_node.getAttribute("lang")
						key_att = text_node.getAttribute("key")
					

						if (cmp(lang, "") != 0) :
							if (lang not in activity.active_langs) :
								print data_file + " has an entry in " + lang + " language but the activity has no title in this language"
						else :
							if (cmp(default_lang,"") != 0) :
								# we're inside a single language file
								# so we assume the language of the text is the same as the title
								lang = default_lang
							else :
								if (cmp(key_att, "") == 0) :
									print data_file + " has an entry "+text+" with no language attribute but the activity has multiple languages"


						activity.add_text(lang, text)




				except :
					print data_file + " seems to have an incorrect format"

					print traceback.format_exc()




			size = os.path.getsize(data_file)

			total_multimedia_files = total_multimedia_files + 1

			if (mysql == True) :
				# insert file data

				cursor.callproc("Insert_file", (data_file, path, filename, extension, media_type, size, par_file_original_URL, par_file_license_URL, par_author_name, par_license_name))

				##### sys.stdout.write("#")
				sys.stdout.flush()

				
				# insert language information on this file
				for language in file_languages :
					cursor.callproc("file_in_language", (data_file, language))

					##### sys.stdout.write("%")

					sys.stdout.flush()


mediafiles.print_mediafiles()

activities.print_active_langs()

activities.print_lang_stats()

print
print "Done"

print
print "Total amount of multimedia files " + str(total_multimedia_files)
print "Total amount of multimedia files with license information " + str(total_multimedia_files_with_lic_info) + " ("+ str(round((total_multimedia_files_with_lic_info/total_multimedia_files) * 100)) + ")"
print

if (mysql == True) :
	cursor.close()
	