import sys
import os

from PySide import QtGui


class ButtonLabel(QtGui.QLabel):
    def __init__(self, pixmap):
        QtGui.QLabel.__init__(self)
        self.setPixmap(pixmap)
        self.effect = QtGui.QGraphicsDropShadowEffect()
        self.setGraphicsEffect(self.effect)
        self.effect.setEnabled(False)

    def enterEvent(self, event):
        self.effect.setEnabled(True)
        pass

    def leaveEvent(self, event):
        self.effect.setEnabled(False)
        pass

    def mousePressEvent(self, event):
        pass


class MainWindow(QtGui.QWidget):
    def __init__(self):
        QtGui.QWidget.__init__(self)

        pixmap = QtGui.QPixmap(os.path.join('..', 'data', 'Omnitux2_logo.png'))

        button = ButtonLabel(pixmap)
        layout = QtGui.QVBoxLayout()
        layout.addWidget(button)
        self.setLayout(layout)


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)

    window = MainWindow()
    window.show()

    sys.exit(app.exec_())